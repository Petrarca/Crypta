# Crypta

## gocryptfs
The gocryptfs library is probably the best solution on linux currently when someone wants to encrypt folders. To check for details, how to install it and how to initialize an encrypted folder, just check [gocryptfs homepage](https://nuetzlich.net/gocryptfs/).

## Prerequisites
The script makes use of the following programs: `gocryptfs` (obviously), `mountpoint`, `zenity`, `notify-send`, and `xdg-open`. If they're not available on your system yet either install them or look for alternatives and replace them in the script.

If your using `firejail` as well then there is an issue with FUSE filesystems. To overcome this, gocryptfs (as well as all other filesystems like sshfs) must be run with the option `-allow_other`. In addition the FUSE config in `/etc/fuse.conf` must include `user_allow_other`.

## Shell script
The shell script can be just used directly from the command-line (terminal). To do so, just make it executeable - `chmod u+x script.sh` - and run it from the shell.

## ulauncher shortcut

If your using ulauncher you can easily create a shortcut within ulauncher to mount and unmount your encrypted folder.

To include the script into ulauncher, open ulauncher, go to the settings (small gear-symbol on the right), then go to  'shortcuts', create a new one and fill out the fields along the lines of this screenshot:

![ulauncher Screenshot](Screenshot.png "ulauncher Screenshot")

You can change the name of the script or the keyword (the actual shortcut being used; the letter `c` in the above screenshot) to you preferences. The icon provided in this repository can also bee uploaded into the ulauncher shotcut (not mandatory but a nice thing to have).